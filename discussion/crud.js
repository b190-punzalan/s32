const http = require("http");

const port = 4000;

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"	
	}
];

// storing the createServer method inside the server variable
const server = http.createServer((request, response)=>{
	// route for returning all items upon receiving a GET method request
	// sets the response output to JSON data type
	if(request.url==="/users"&&request.method==="GET"){
		response.writeHead(200, {"Content-Type": "application/json"});
		response.write(JSON.stringify(directory));
		// response.write() - write any sepcified string
		// since the needed data type is string, we use JSON.stringify to "write" it as our response. this is done because requests and respnses sent between client and server required the info to be sent and received as a stringified JSON

		response.end();
	};

	if(request.url==="/users"&&request.method==="POST"){
		let requestBody="";
		/*
		A stream is a sequence of data. Data is received from the client and is processed in the "data" stream
		The info provided from the request object enters a sequence called "data" the code below woll be triggered
		- "data" step - this reads the "data" stream and processes it as the request body
		*/
		// this below assigns the data retrieved from the data stream to requestBody
		request.on("data", function(data){
			requestBody+=data;
		});
		request.on("end", function(){
			// checks if at this point, the requestBody is off data type STRING
			// we need this to be of data type JSON to access its properties
			console.log(typeof requestBody);
			// to convert the string requestBody to JSON:
			requestBody=JSON.parse(requestBody);

			// create a new object representing the new mock database record:
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			// Adds the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {"Content-Type": "application/json"});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	};
});

server.listen(port);
console.log(`Server now running at port: ${port}`);

// we use response.end to denote the last stage of the communication 