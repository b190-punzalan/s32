// importing the "http" module

const http = require("http");

const port = 4000;

const server = http.createServer((request, response)=>{
	// GET Request
	if (request.url==="/items"&& request.method==="GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database");
	}
	// POST REQUEST:
	if (request.url==="/items"&&request.method==="POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	}
});

server.listen(port);
console.log(`Server now running at port: ${port}`);

/*
HTTP method can be accessed via "method" property inside the "request" object
*/
// For the GET method, if one of the HTTPS method that we will be using from this point. 
/*
GET Method means that we will be retrieving or reading info 
*/
// Make sure capslock ang mga method kasi di mababasa pag small letters

// POSTMAN
/*
- since the Node js application that we are building is a backend application and there is no fronend application yet to process the requests, we will be using postman to process the request
*/